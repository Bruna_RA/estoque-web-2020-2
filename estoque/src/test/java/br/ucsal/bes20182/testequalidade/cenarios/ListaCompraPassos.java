package br.ucsal.bes20182.testequalidade.cenarios;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ListaCompraPassos {

	private static WebDriver driver;

	@Given("estou na lista de compras")
	public void estouNaListaDeCompras() {
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\bruna\\git\\estoque-web-2020-2\\estoque\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.get("file:///C:/Users/bruna/git/estoque-web-2020-2/estoque/src/main/webapp/lista-compras.html");
		
	}
	
	@When("seleciono o produto $produto")
	public void selecionoOProduto(String produto) {

		WebElement pesquisarNoSiteOption = driver.findElement(By.id("listaProdutos"));

		Select select = new Select(pesquisarNoSiteOption);
		
		select.selectByVisibleText(produto);

	}

	@When("informo a quantidade $quantidade")
	public void informoAQuantidade(String quantidade) {

		WebElement pesquisarNoSiteInput = driver.findElement(By.id("quantidade"));

		pesquisarNoSiteInput.sendKeys(quantidade);

	}

	@When("informo o valor unitário $valorUnitario reais")
	public void informoOValorUnitario(String valorUnitario) {

		WebElement pesquisarNoSiteInput = driver.findElement(By.id("valorUnitario"));

		pesquisarNoSiteInput.sendKeys(valorUnitario);

	}

	@When("confirmo a compra")
	public void confirmoACompra() {

		WebElement confirmacao = driver.findElement(By.id("calcularBtn"));

		confirmacao.click();

	}

	@Then("terei de pagar $valorTotal")
	public void tereiDePagar(String valorTotal) {

		WebElement totalInput = driver.findElement(By.id("valorTotal"));
		Assert.assertEquals(valorTotal, totalInput.getAttribute("value").concat(" reais"));
		driver.quit();
		
	}

}
