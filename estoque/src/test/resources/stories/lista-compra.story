Cenário: Comprar manga
Dado que estou na lista de compras
Quando seleciono o produto Manga
E informo a quantidade 12
E informo o valor unitário 6 reais
E confirmo a compra
Então terei de pagar 72 reais

Cenário: Comprar jaca
Dado que estou na lista de compras
Quando seleciono o produto Jaca
E informo a quantidade 17
E informo o valor unitário 2 reais
E confirmo a compra
Então terei de pagar 34 reais
